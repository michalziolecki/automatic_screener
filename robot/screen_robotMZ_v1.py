import keyboard
from PIL import ImageGrab
import datetime
import os


class KeystrokeWatcher(object):
    def __init__(self):
        keyboard.hook(self.on_keyboard_event)

    def on_keyboard_event(self, event):
        try:
            if keyboard.is_pressed('f2'):
                self.screen_shot()
        finally:
            return True

    def screen_shot(self):
        snapshot = ImageGrab.grab()
        now_date = datetime.datetime.now().strftime('%Y-%m-%d')
        now_date_time = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        save_path = ''.join(['./', now_date, '/', now_date_time, '.jpg'])
        print("-> screen shot done: " + now_date_time + '.jpg')
        if not os.path.isdir(''.join(['./', now_date])):
            os.mkdir(''.join(['./', now_date]))
        snapshot.save(save_path)


def main():
    print("Application started :) \n - press 'F2' to do screen \n - press 'crtl' + 'q' to exit")
    try:
        watcher = KeystrokeWatcher()
        keyboard.wait(hotkey='ctrl+q')
    except KeyboardInterrupt:
        pass
    except BaseException as e:
        err = "There was an exception in screen_shot method"
        print(err + '\n' + e.__context__)


if __name__ == '__main__':
    main()
